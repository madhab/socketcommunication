package test.net.socket;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.Scanner;

public class GreetingClient {

	public static void main(String[] args) throws InterruptedException {
		
		System.out.println("");
		System.out.println(" ***** Greetings Socket Client ***** ");
		System.out.println("");
		
		String serverName = "";
		int port = -1;
		
		
		Scanner input = null;
		try {
			System.out.print("Please enter server name: ");
			input = new Scanner(System.in);
			serverName = input.next();
			
			System.out.print("Please enter server port: ");
			port = input.nextInt();
		} catch (Exception e) {} finally { if(input != null) { input.close(); } }
		
		if(serverName.length() > 0 && port > -1) {
			try {
				System.out.println("Connecting to " + serverName + " on port " + port);
				
				while(true) {
					Socket client = new Socket(serverName, port);
					System.out.println("Just connected to " + client.getRemoteSocketAddress());
					
					OutputStream outToServer = client.getOutputStream();
					DataOutputStream out = new DataOutputStream(outToServer);
					
					//out.writeUTF("Hello from: >>> " + client.getLocalSocketAddress());
					System.out.println("");
					out.writeUTF(String.valueOf(Math.random()));
					System.out.println("");
					
					InputStream inFromServer = client.getInputStream();
					DataInputStream in = new DataInputStream(inFromServer);
					System.out.println("Server says: " + in.readUTF());
					
					Thread.sleep(2000);
					
					client.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
				System.err.println("Error: " + e.getMessage());
			}
		} else {
			System.err.println("Invalid server name: " + serverName + ", or port: " + port);
			System.err.println("Please enter valid server name and port!");
		}
	}
}
