package test.net.socket;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.util.Scanner;

/**
 * Greeting Server!
 *
 */
public class GreetingServer extends Thread {
	
	private ServerSocket serverSocket;

	public GreetingServer(int port) throws IOException {
		serverSocket = new ServerSocket(port);
		serverSocket.setSoTimeout(30 * 60 * 1000);
	}

	public void run() {
		while (true) {
			try {
				System.out.println("");
				System.out.println("Waiting for client on port " + serverSocket.getLocalPort() + " ...");
				System.out.println("");
				Socket server = serverSocket.accept();
				
				System.out.println("Just connected to " + server.getRemoteSocketAddress());
				System.out.println("");
				
				DataInputStream in = new DataInputStream(server.getInputStream());
				System.out.println(in.readUTF());
				
				DataOutputStream out = new DataOutputStream(server.getOutputStream());
				out.writeUTF("Thank you for connecting to " + server.getLocalSocketAddress() + "\nGoodbye!");
				server.close();
			} catch (SocketTimeoutException s) {
				s.printStackTrace();
				System.err.println("Socket timed out! " + s.getMessage());
				break;
			} catch (IOException e) {
				e.printStackTrace();
				System.err.println(e.getMessage());
				break;
			}
		}
	}

	public static void main(String[] args) {
		
		System.out.println("");
		System.out.println(" ***** Greetings Socket Server ***** ");
		System.out.println("");
		
		System.out.print("Please enter server port: ");
		
		int port = -1;
		Scanner input = null;
		try {
			input = new Scanner(System.in);
			port = input.nextInt();
		} catch (Exception e) {} finally { if(input != null) { input.close(); } }
		
		if(port > 0) {
			try {
				Thread t = new GreetingServer(port);
				t.start();
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else {
			System.err.println("Please enter valid port!");
		}
	}
}
